from tkinter import *
from tkinter import messagebox

class Window(Frame):
    def __init__(self,master=None):
        Frame.__init__(self,master);
        self.master=master;
        self.init_window();

    def init_window(self):
        #chnage title of the window
        self.master.title("GUI");
        #allow GUI object tot ake fulll space of Window
        self.pack(fill=BOTH,expand=1);

        menu=Menu(self.master);
        self.master.config(menu=menu);
        file=Menu(menu);
        file.add_command(label="Exit",command=self.client_exit);
        menu.add_cascade(label="File",menu=file);
        edit=Menu(menu);
        #menu.add_cascade(label="Edit",menu=edit);
        edit.add_command(label="Undo");
        menu.add_cascade(label="Edit",menu=edit);

    def client_exit(self):
        exit();

    def wish_click(self):
        messagebox.showinfo("title","Hello");


root=Tk();
root.geometry("1366x768")
app=Window(root);
root.mainloop();
